## Web Developer Assessment

This assessment is designed to test the technical skills of prospective web developers. To get started, download this repository to your computer. You can do this in one of three ways:

-   Fork this repository. Submit your finished assessment via a pull request.
-   **Extra Credit**: Send a url so we can see your work live on the internet!

#### Problem

We are looking to add some new content to our [Space theme](https://new.americanreading.com/themes/space) for ARC Core. Our curriculum and instruction team would love to see some data about rockets. 🚀

#### Task

Use the SpaceX API to display the last 50 launches and some info about each one.

-   Rocket
    -   name
    -   weight(lb)
    -   cost per launch
-   Launch Site
-   Launch Date
-   Mission Name
-   Flight Number
-   Details

##### Things to think about:

-   How can you make the data interactive?
-   Is the UI consistent and user-friendly?
-   Is the intention/function of the page readily apparent to the user?
-   Does the page look good on all devices?

You can find the docs for the API here: [https://docs.spacexdata.com/](https://docs.spacexdata.com/)

You can use any tools you wish to complete this assessment. Feel free to use the internet to look for resources!

Tailwind CSS is already set up and ready to go in the project.

##### To Get Started

Install all dependencies

    npm install

##### To Start

Run the dev server

    npm start

##### To Build

Build the project for production

    npm build
