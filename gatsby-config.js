module.exports = {
	siteMetadata: {
		title: 'Gatsby Space'
	},
	plugins: ['gatsby-plugin-postcss']
}
